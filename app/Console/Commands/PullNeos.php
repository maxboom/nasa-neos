<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Nasa\Api\NeoApi;
use App\Models\Neo;

class PullNeos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nasa:neos {--start_date=3 days ago} {--end_date=now}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $_neoApi = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NeoApi $neoApi)
    {
        parent::__construct();

        $this->_neoApi = $neoApi;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = $this->option('start_date');
        $endDate = $this->option('end_date');

        $neos = $this->_neoApi->feed($endDate, $startDate);

        $this->info("Elements found: $neos[element_count]");

        $neos = collect($neos['near_earth_objects'])
            ->map(function ($neosPerDay, $date) {
                return collect($neosPerDay)
                    ->map(function($neo) use ($date) {
                        return Neo::firstOrNew([
                            'reference' => $neo['neo_reference_id']
                        ])->forceFill([
                            'date' => $date,
                            'name' => $neo['name'],
                            'is_hazardous' => $neo['is_potentially_hazardous_asteroid'],
                            'speed' => array_shift($neo['close_approach_data'])
                                           ['relative_velocity']
                                           ['kilometers_per_hour']
                        ])->save();
                    });

            }
        );

        $this->info("Pulling is successful complited");
    }
}
