<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Neo;

class NeoController extends Controller
{
    public function hazardous()
    {
        return Neo::whereIsHazardous(true)->get();
    } 

    public function fastest()
    {
        // $isHazardous = (request()->input('hazardous') === 'true');
        // i don't know if i have to compare this value strictly, but i leave it here.

        $isHazardous = filter_var(
            request()->input('hazardous'),
            FILTER_VALIDATE_BOOLEAN
        );

        return Neo::whereIsHazardous($isHazardous)
            ->orderBy('speed', 'desc')
            ->first();

    }
    
}
