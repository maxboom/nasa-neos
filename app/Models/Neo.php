<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Neo extends Model
{
    protected $fillable = [
        'date',
        'reference',
        'name',
        'speed',
        'is_hazardous'
    ];

    protected $dates = [
        'date'
    ];
}
