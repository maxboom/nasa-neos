<?php

namespace App\Services\Nasa\Api;

use App\Services\Nasa\Exceptions\NeoApiException;
use cURL;

class NeoApi
{
    private $_apiKey = null;

    private $_feedUrl = 'https://api.nasa.gov/neo/rest/v1/feed';

    public function __construct($apiKey)
    {
        $this->_apiKey = $apiKey;
    }

    public function feed($endDate, $startDate = null)
    {
        $startDate = ($startDate) ? date_create($startDate) : date_create('now');
        $endDate = date_create($endDate);

        $response = cURL::get("$this->_feedUrl?" . http_build_query([
                    'start_date' => $startDate->format('Y-m-d'),
                    'end_date' => $endDate->format('Y-m-d'),
                    'api_key' => $this->_apiKey
                ]
            )
        );

        $data = json_decode($response->body, true);

        if (($error = $this->_hasErrors($data))) {
            $errorMessage = __('nasa::exceptions.neo_exception', $error);
            throw new NeoApiException($errorMessage);
        }

        return $data;
    }

    private function _hasErrors($data)
    {
        if (array_key_exists('error', $data)) {
            return $data['error'];
        }

        if (array_key_exists('error_message', $data)) {
            return [
                'code' => $data['code'],
                'message' => $data['error_message'] 
            ];
        }
    }
}