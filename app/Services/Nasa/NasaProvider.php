<?php

namespace App\Services\Nasa;

use Illuminate\Support\ServiceProvider;
use App\Services\Nasa\Api\NeoApi;

class NasaProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/resources/translations', 'nasa');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NeoApi::class, function ($app) {
            return new NeoApi(config('nasa.api_key'));
        });
    }
}
