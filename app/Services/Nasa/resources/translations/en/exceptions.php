<?php

return [
    'neo_exception' => 'Request finished unsuccessfully with code :code explaining that :message'
];