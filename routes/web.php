<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return ['hello' => 'world'];
});

Route::get(
    '/neo/hazardous', 
    'NeoController@hazardous'
)->name('neo_hazardous');

Route::get(
    '/neo/fastest',
    'NeoController@fastest'
)->name('neo_fastest');